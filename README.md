# AuthRedisHandler

Module for authorization verification. 
## Installation

<code>$ npm install auth-redis-handler-nt --save </code>

## Documentation

- <code>configure(obj)</code><br>
	Set configuration object before using other functions.
	
	<b>It also set the client of Redis.</b><br>
	Example : 
	```json
	{
		"JWT_TOKEN": "TESTJWT",
		"API_KEY": "SUPERAPIKEY"
	}
	````

	
	
- <code>checkAPIKey(req, res, next)</code><br>
	Middleware for API KEY checking.<br>
	Returns error with status 401 if wrong API KEY.
	
- <code>checkJWT(req, res, next)</code><br>
	Middleware for JWT checking.<br>
	Returns error with status 401 if wrong JWT.
	
- <code>createJWT_HMAC(exp, data)</code><br>
	Return JWT using HMAC algorithm.
	
- <code>createSession(UserID, JWT</code><br>
	Set session in Redis.
	
- <code>verifySession(UserID, JWT)</code><br>
	Verify if UserID as JWT and is the same.

- <code>deleteSession(UserID)</code><br>
	Delete JWT (session) of the UserID in Redis. 
