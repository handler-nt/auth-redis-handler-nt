const jwt = require('jsonwebtoken');
const errorHandler = require('error-handler-nt');
let client = null;


let config = null;

/**
 * Add config
 * @param {Object} obj
 */
function configure(obj)
{
  config = obj;
  client = require('redis-handler-nt').getClient();
}


/**
 * Check API Key
 * @param {*} req
 * @param {*} res
 * @param {callback} next
 * @return {*}
 */
function checkAPIKey(req, res, next)
{
  if (!config)
    return next(errorHandler.create(500, "No configuration set", "AuthHandler - checkAPIKEY"));

  if (req.query.hasOwnProperty("api_key"))
  {
    if (req.query.api_key === config.API_KEY)
      return next();

    return next(errorHandler.create(401, "API KEY error"));

  }

  return next(errorHandler.create(401, "API KEY not found"));
}

/**
 * Check JsonWebToken
 * @param {Object} req
 * @param {Object} res
 * @param {callback} next
 * @return {*}
 */
function checkJWT(req, res, next)
{
  if (!config)
    return next(errorHandler.create(500, "No configuration set", "AuthHandler - checkJWT"));

  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === "Bearer")
  {
    const JWT = req.headers.authorization.split(' ')[1];

    jwt.verify(JWT, config.JWT_TOKEN, async (err, decode) =>
    {
      if (err)
      {
        if (err.name === "TokenExpiredError")
          return errorHandler.createAndSend(res, 401, {message: "Token expired", type: "EXPIRED_TOKEN"});

        return errorHandler.createAndSend(res, 401, {message: "Wrong token", type: "BAD_TOKEN"});
      }

      try
      {
        if (decode.data.id === "x01" || await verifySession(decode.data.id, JWT) === true)
        {
          req['user'] = decode.data;
          return next();
        }

        return errorHandler.createAndSend(res, 401, {message: "Token expired", type: "INVALID_SESSION"});
      }
      catch (err)
      {
        return errorHandler.createAndSend(res, 500);
      }
    })
  }
  else
    return errorHandler.createAndSend(res, 401, {message: "No token found", type: "NO_TOKEN"});
}

/**
 * Create JWT using HMAC
 * @param exp
 * @param data
 * @return {*}
 */
function createJWT_HMAC(exp, data)
{
  return jwt.sign(
  {
    exp: exp,
    data: data
  },
  config.JWT_TOKEN);
}


/***
 * CreateSession
 * @param UserID
 * @param JWT
 * @return {void}
 */
function createSession(UserID, JWT)
{
  return new Promise((resolve, reject) =>
  {
    try
    {
      const key = `Sessions_JWT_${UserID.toString()}`;

      client.hmset(key, JWT, Date.now().toString(), function(err, reply)
      {
        if (err)
          return reject(err);

        return resolve(true);
      });

    }
    catch (err)
    {
      return reject(err);
    }
  });
}


/**
 * verifySession
 * @param UserID
 * @param JWT
 * @return {Promise<any>}
 */
function verifySession(UserID, JWT)
{
  return new Promise((resolve, reject) =>
  {
    try
    {
      const key = `Sessions_JWT_${UserID.toString()}`;

      client.hgetall(key, (err, results) =>
      {
        if (err)
          return reject(false);

        if (results)
          return resolve(results.hasOwnProperty(JWT));
        else
          return resolve(false);
      });

    }
    catch (err)
    {
      return reject(err);
    }
  });
}

/**
 * DeleteSession
 * @param UserID
 * @return {void}
 */
function deleteSession(UserID)
{
  client.del(`Sessions_JWT_${UserID}`);
}

module.exports =
{
  configure,
  checkAPIKey,
  checkJWT,
  createJWT_HMAC,
  
  createSession,
  verifySession,
  deleteSession
};
