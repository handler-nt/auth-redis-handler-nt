const assert = require('assert');
const redisHandler = require('redis-handler-nt');
let lib = null;

describe('Configuration', () =>
{
  it("Should not fail", async () =>
  {
    await redisHandler.connect();
    lib = require('../index.js');
    lib.configure({JWT_TOKEN: "TESTJWT", API_KEY: "SUPERAPIKEY"});
  });
});

describe("API KEY", () =>
{
  it("Fail : Verification of API KEY", () =>
  {
    lib
    .checkAPIKey({query: {api_key: "XXX"}}, null, (obj) =>
    {
      assert.notEqual(typeof obj, 'undefined');
    });
  });

  it("Verification of API KEY", () =>
  {
    lib
    .checkAPIKey({query: {api_key: "SUPERAPIKEY"}}, null, (obj) =>
    {
      assert.equal(typeof obj, 'undefined');
    });
  });
});


describe('Check JWT', () =>
{

  it("Fail : Verification of JWT", () =>
  {
    lib
    .checkJWT({headers: {authorization: "Bearer "+"XXX"}}, null, (obj) =>
    {
      assert.notEqual(typeof obj, 'undefined');
    })
  });

  it("Verification of JWT", () =>
  {
    const exp = Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 10);

    let jwt = lib
    .createJWT_HMAC(
    exp,
    {
      id: 1,
      role: "admin",
      entity_id: "x01"
    });

    lib.createSession(1, jwt);

    lib
    .checkJWT({headers: {authorization: "Bearer "+jwt}}, null, (obj) =>
    {
      assert.equal(typeof obj, 'undefined');
    })
  });


  it("Verification of delete session JWT", () =>
  {
    const exp = Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 10);

    let jwt = lib
    .createJWT_HMAC(
    exp,
    {
      id: 1,
      role: "admin",
      entity_id: "x01"
    });

    lib.deleteSession(1);

    lib
    .checkJWT({headers: {authorization: "Bearer "+jwt}}, null, (obj) =>
    {
      assert.notEqual(typeof obj, 'undefined');
    })
  });
});
